
 1. In the first window, build the application using `mvn clean package`
 2. In the same window run: `java -jar target/microservice-demo-1.1.0.RELEASE.jar registration` and wait for it to start up
 3. Switch to the second window and run: `java -jar target/microservice-demo-1.1.0.RELEASE.jar discount` and again wait for
 it to start up
 
 4. In browser open the same two links: [http://localhost:1111](http://localhost:1111) and [http://localhost:4444](http://localhost:4444)

You should see server i.e discount being registered in the log output of the first (registration) window.


