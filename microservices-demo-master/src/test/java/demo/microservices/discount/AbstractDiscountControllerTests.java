package demo.microservices.discount;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import demo.microservices.services.discount.Discount;
import demo.microservices.services.discount.DiscountController;

public abstract class AbstractDiscountControllerTests {

	@Autowired
	DiscountController discountController;
	
	private static final String PROD_ID = "sku-1234567890";
	private static final String USER_ID = "qa-test-user";
	
	private static final String BAD_PROD_ID = "xxx";
	private static final String BAD_USER_ID = "uuu";

	@Test
	public void validDiscountList() {
		List<Discount> discount = discountController.retrieveDiscount(USER_ID);

		Assert.assertNotNull(discount);
	}
	
	@Test
	public void validDiscountListForProdId() {
		Discount discount = discountController.retrieveDiscountForProductId(PROD_ID);

		Assert.assertNotNull(discount);
		Assert.assertEquals(discount.getProductId(),PROD_ID);
	}
	
	@Test
	public void inValidDiscountList() {
		List<Discount> discount = discountController.retrieveDiscount(BAD_USER_ID);

		Assert.assertNull(discount);
	}
	
	@Test
	public void inValidDiscountListForProdId() {
		Discount discount = discountController.retrieveDiscountForProductId(BAD_PROD_ID);

		Assert.assertNull(discount);
	}
	
}
