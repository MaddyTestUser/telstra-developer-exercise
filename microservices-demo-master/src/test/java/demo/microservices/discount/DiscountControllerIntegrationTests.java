package demo.microservices.discount;

import org.junit.runner.RunWith;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import demo.microservices.services.discount.DiscountServer;

@SpringBootApplication
class DiscountMain {
	public static void main(String[] args) {
		// Tell server to look for discount-server.properties or
		// discount-server.yml
		System.setProperty("spring.config.name", "discount-server");
		SpringApplication.run(DiscountServer.class, args);
	}
}


@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DiscountServer.class)
public class DiscountControllerIntegrationTests extends AbstractDiscountControllerTests {

}
