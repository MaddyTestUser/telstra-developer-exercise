package demo.microservices.services.discount;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import demo.microservices.services.exceptions.ErrorResource;
import demo.microservices.services.exceptions.UserNotFoundException;
import net.minidev.json.JSONObject;


@Service
public class DiscountService {

	@Autowired
	@LoadBalanced
	protected RestTemplate restTemplate;

	protected String serviceUrl;
	final private static Logger logger = LoggerFactory.getLogger(DiscountService.class);

	private static final String ELIGIBLE_DISCOUNT = "eligibleDiscounts";
	private static final String URL = "/rest/v1/customers/";
	private static final String AMT ="amount";
	private static final String DESC = "description";
	private static final String PROD_ID = "productId";
	private static final String DISCOUNT_ID = "discountId";
	private static final String TYPE = "type";

	public DiscountService(){}
	public DiscountService(String serviceUrl) {
		this.serviceUrl = serviceUrl.startsWith("http") ? serviceUrl
				: "http://" + serviceUrl;
		logger.info("The serviceUrl : "+serviceUrl);
	}

	/**
	 * The RestTemplate works because it uses a custom request-factory that uses
	 * Ribbon to look-up the service to use. This method simply exists to show
	 * this.
	 */
	@PostConstruct
	public void demoOnly() {
		// Can't do this in the constructor because the RestTemplate injection
		// happens afterwards.
		logger.info("TestService ::::The RestTemplate request factory is "
				+ restTemplate.getRequestFactory().getClass());
	}

	/**
	 * This method will call rest api /rest/v1/customers/qa-test-user and then extract the discount data
	 * and return
	 * @return
	 */
	public List<Discount> retrieveDiscount(String userId) {
		logger.info("retrieveDiscount()  invoked:" );
		// Call the API to get customer json
		//TODO : This url should be put in a property file  & shud be picked from there.
		JSONObject customerJson;
		try{
		customerJson = restTemplate.getForObject(serviceUrl + URL + "{userId}",	JSONObject.class);
		}catch(HttpClientErrorException e){
			if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
		        logger.debug("User not found for " + userId);
		        throw new UserNotFoundException("Bad request - user not found in the system");
		    }else {
		        logger.debug("returning other exception:" + e.toString());
		        throw e;
		    }
		}
		//Extract the discount json and convert to discount list
		return extractDiscountFromCustomerJson(customerJson);		
	}
	/**
	 * This method will call rest api for particular product id e:g
	 *  http://52.65.9.120:9999/rest/v1/users/qa-test-user/discounts?productId=sku-1234567890
	 * @param productId
	 * @return
	 */
	public List<Discount> retrieveDiscountForProductId(String productId) {
		logger.info("retrieveDiscount()  invoked:" );
		// Call the API to get customer json
		//TODO : This url should be put in a property file e & shud be picked from there.
		JSONObject customerJson;
		try{
			customerJson = restTemplate.getForObject(serviceUrl+ URL + "{productId}" ,	JSONObject.class);
		}catch(HttpClientErrorException e){
			if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
		        logger.debug("User not found for " + productId);
		        throw new UserNotFoundException("Bad request - product not found in the system");
		    }else {
		        logger.debug("returning other exception:" + e.toString());
		        throw e;
		    }
		}
		//Extract the discount json & convert to discount list
		return extractDiscountFromCustomerJson(customerJson);		
	}
	
	@ExceptionHandler(UserNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ResponseEntity<ErrorResource> exceptionHandler(UserNotFoundException ex) {
	    ErrorResource error = new ErrorResource();
	    error.setErrorCode(ex.getErrorCode());
	    error.setMsg(ex.getMessage());
	    return new ResponseEntity<ErrorResource>(error, HttpStatus.NOT_FOUND); 
	}
	
	

	/**
	 * Method to extract the discount Json and convert it to Discount type.
	 * @return
	 */
	private List<Discount> extractDiscountFromCustomerJson(JSONObject customerJson){
		
		@SuppressWarnings("unchecked")
		List<Map<String,Object>> discountListMap = (ArrayList<Map<String,Object>>) customerJson.get(ELIGIBLE_DISCOUNT);
		if (null == discountListMap){
			logger.error("No data in the json!"); 
			return Collections.emptyList();
		}
		List<Discount>discountList = new ArrayList<Discount>();
		for (Map<String,Object> data : discountListMap){
			Discount discount = new Discount();
			discount.setAmount(new BigDecimal((String)data.get(AMT)));
			discount.setDescription((String)data.get(DESC));
			discount.setDiscountId((String)data.get(DISCOUNT_ID));
			discount.setProductId((String)data.get(PROD_ID));
			discount.setType((String)data.get(TYPE));	
			
			discountList.add(discount);
		}
		return discountList;
	}
}
