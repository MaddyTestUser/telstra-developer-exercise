package demo.microservices.services.discount;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("Discount")
public class Discount {

	protected String discountId;
	protected String type;
	protected String description;
	protected BigDecimal amount;
	protected String productId;
	
	

	public String getProductId() {
		return productId;
	}

	public void setProductId(String product) {
		this.productId = product;
	}

	/**
	 * Default constructor for JPA only.
	 */
	protected Discount() {
		amount = BigDecimal.ZERO;
	}

	public String getDiscountId() {
		return discountId;
	}


	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDiscountId(String discountId) {
		this.discountId = discountId;
	}

	public BigDecimal getAmount() {
		return amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
	}

	protected void setAmount(BigDecimal value) {
		amount = value;
		amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
	}

	@Override
	public String toString() {
		return type + " [" + description + "]: $" + amount;
	}

}
