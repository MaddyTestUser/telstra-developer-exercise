package demo.microservices.services.discount;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

//import com.google.gson.JsonArray;

@Controller
public class DiscountController {

	@Autowired
	protected DiscountService discountService;

	protected Logger logger = Logger.getLogger(DiscountController.class
			.getName());

	public DiscountController(DiscountService discountService) {
		this.discountService = discountService;
	}

	@RequestMapping("/rest/v1/users/{userId}/discounts")
	public List<Discount> retrieveDiscount(@PathVariable("userId")String userId) {

		logger.info("retrieveDiscount() invoked: " );

		List<Discount> discount = discountService.retrieveDiscount(userId);
		logger.info("Number of Discount found: " + discount.size());
		//TODO : Need to inplement exception handling to handle null json
		return discount;
	}
	
	@RequestMapping("/rest/v1/users/qa-test-user/discounts/{productId}")
	public Discount retrieveDiscountForProductId(@PathVariable("productId")String productId) {

		logger.info("retrieveDiscountForProductId() invoked: " );

		List<Discount> discount = discountService.retrieveDiscountForProductId(productId);
		//TODO : Need to inplement exception handling to handle empty json
		logger.info("Number of Discount found: " + discount.size());
		if (null != discount && ! discount.isEmpty()){
			return discount.get(0);
		}
		return new Discount();
	}
	

}
