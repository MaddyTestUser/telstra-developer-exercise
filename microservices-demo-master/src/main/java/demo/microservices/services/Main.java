package demo.microservices.services;

import demo.microservices.services.discount.DiscountServer;
import demo.microservices.services.registration.RegistrationServer;


public class Main {

	public static void main(String[] args) {

		String serverName = "NO-VALUE";

		switch (args.length) {
		case 2:
			// Optionally set the HTTP port to listen on, overrides
			// value in the <server-name>-server.yml file
			System.setProperty("server.port", args[1]);
			// Fall through into ..

		case 1:
			serverName = args[0].toLowerCase();
			break;

		default:
			System.out.println("Unknown server type: " + serverName);
			return;
		}

		if (serverName.equals("registration") || serverName.equals("reg")) {
			System.out.println("registration.....");
			RegistrationServer.main(args);
		} else if (serverName.equals("discount")) {
			System.out.println("discount....");
			DiscountServer.main(args);
		} 
		else {
			System.out.println("Unknown server type: " + serverName);
		}
	}

}
